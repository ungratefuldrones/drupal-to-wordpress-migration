## Where Things are 

Drupal Content | WP Type | WP Taxonomy | WP Meta Type
---|---|---|---
article | post | - | postmeta
image | post | - | postmeta
program | term | program | -
author | term | author_tag | termmeta
tag | term | post_tag | -

### Metadata

**_nid** - Drupal nid 

**wamu_audio** - Contains a path to the audio file for a story; the root is http://downloads.wamu.org

**url_audio_feed** - Audio url for a story retrieved from the NPR API

**image_feed_url_wide** - Image url for a story retrieved from the NPR API

**image_feed_caption** - Image caption for a story retrieved from the NPR API

**image_feed_title** - Image title for a story retrieved from the NPR API

**image_feed_producer** - Image producer for a story retrieved from the NPR API

**image_feed_provider** - Image url for a story retrieved from the NPR API

**image_feed_copyright** - Image copyright for a story retrieved from the NPR API

**extra_content** - Usually contains embeds or external scripts.  May contain Markdown.

**subtitle** - Subtitle for a story

**author_name** - One or more free text author names 

**link_related_title** - One or more related link titles

**link_related_url**  - One or more related link url's



## Requirements 
* SQLAlchemy
* dateutil
* python-slugify

## Usage
Edit config.py and add connection strings for the databases to be used for the migration in the following pattern:
```
mysql://user:password@host:port/database_name
```

Run the migration:
```
python main.py
```

Consider running with PyPy

## To Do 
* Organization taxonomy
* Location taxonomy
* Automated cleaning, parsing, rendering of Markdown, etc.
* Redirects
* Disqus
* Open Graph
* API image downloading and attachment
* Related articles
* Update in place
* Performance improvements


## Bugs
* Warnings