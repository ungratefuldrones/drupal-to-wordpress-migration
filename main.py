import sqlalchemy as sa
import wpalchemy.classes as wp
import d7alchemy.classes as d7
from datetime import *
from dateutil.tz import *
from dateutil.parser import parse
from slugify import slugify
import config

# Setup Drupal 7 session
d7_engine = sa.create_engine(config.drupal_connection_string)
d7_session = sa.orm.sessionmaker(d7_engine)()

# m:n things being handled as taxonomies at the moment
programs = {}
program_exclusion_list =[]
author_exclusion_list =[]
authors = {}
taxonomy = {}

# check post url alias getting screwed up

def migrate_authors():
	"""
	
	- Add authors to dict 
	- Create exclusion list for authors that have been deleted, but are still referenced in the field_data_field_author table
	- Add to session
	- Flush

	"""
	global authors
	global author_exclusion_list

	author_exclusion_sql = """
		select distinct field_author_nid from field_data_field_author where field_author_nid not in(select nid from node where type='author')
		"""

	author_exclusion_list = [i[0] for i in d7_session.execute(author_exclusion_sql)]

	query = d7_session.query(d7.Node_Author).filter_by(type="author").all()

	authors = {i.nid: wp.PostTag(term=wp.Term(name=i.title, slug=slugify(i.title), term_group=0, meta={'twitter': i.twitter_screen_name, 'job_title': i.jobtitle}), description=i.body, taxonomy="author_tag") for i in query}

	wp.wp_session.add_all([v for (k, v) in authors.items()])
	wp.wp_session.flush()

def migrate_programs():
	"""
	
	- Add programs to dict 
	- Create exclusion list for programs that have been deleted, but are still referenced in the field_data_field_program table
	- Add to session
	- Flush

	"""
	global programs
	global program_exclusion_list

	program_exclusion_sql = """
		select distinct field_program_nid from field_data_field_program where field_program_nid not in(select nid from node where type='programs')
		"""
	program_exclusion_list = [i[0] for i in d7_session.execute(program_exclusion_sql)]

	query = d7_session.query(d7.Node_Program).filter_by(type="programs").all()

	programs = {i.nid: wp.PostTag(term=wp.Term(name=i.title, slug=slugify(i.title), term_group=0), description=i.body, taxonomy="program") for i in query}

	wp.wp_session.add_all([v for (k, v) in programs.items()])
	wp.wp_session.flush()

def migrate_tags():
	"""
	
	- Add programs to dict 
	- Add to session
	- Flush

	"""
	global taxonomy
	term_query = d7_session.query(d7.Taxonomy_Term).distinct(d7.Taxonomy_Term.name).filter(sa.or_(d7.Taxonomy_Term.vid == 1, d7.Taxonomy_Term.vid == 5)).all()

	taxonomy = {i.name: wp.PostTag(term=wp.Term(name=i.name, slug=slugify(i.name), term_group=0), description='', taxonomy="post_tag") for i in term_query}

	wp.wp_session.add_all([v for (k, v) in taxonomy.items()])
	wp.wp_session.flush()

def migrate_articles():
	"""
	
	- Fetch arcticles from Drupal
	- Add to session
	- Flush

	"""
	query = d7_session.query(d7.Node).filter_by(type='article').filter_by(status=1).all()
	
	temp = [wp.Post(post_title=i.title, 
			post_type='post', 
			post_date=i.date_publish, 
			post_date_gmt=i.date_publish_gmt,
			post_content=i.body,
			post_excerpt=i.teaser,
			post_modified=i.date_publish, 
			post_modified_gmt=i.date_publish_gmt,
			guid='',
			post_name=i.url_alias,
			meta=i.meta,
			children=i.children,
			terms=[taxonomy[j] for j in i.tags if j is not None],
			programs=[programs[j] for j in i.program if j is not None and j not in program_exclusion_list],
			authors=[authors[j] for j in i.author if j is not None and j not in author_exclusion_list]	
		) for i in query if i.channel in [12,14]]

	wp.wp_session.add_all(temp)
	wp.wp_session.flush()

def update_taxonomy_count():
	sql = """
		UPDATE wp_term_taxonomy SET count = (
		SELECT COUNT(*) FROM wp_term_relationships rel 
			LEFT JOIN wp_posts po ON (po.ID = rel.object_id) 
		    WHERE 
		        rel.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id 
		        AND 
		        wp_term_taxonomy.taxonomy NOT IN ('link_category')
		        AND 
		        po.post_status IN ('publish', 'future')
		)
		"""	
	wp.wp_session.execute(sql)


if __name__ == '__main__':
	
	print 'STARTING', datetime.now()

	migrate_authors()

	migrate_programs()

	migrate_tags()

	migrate_articles()

	wp.wp_session.commit()

	update_taxonomy_count()

	wp.wp_session.commit()

	print 'FINISHING', datetime.now()



