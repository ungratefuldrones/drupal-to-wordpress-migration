from sqlalchemy import *
from sqlalchemy.orm import *
from config import *
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime
from dateutil.tz import *
from dateutil.parser import parse
from slugify import slugify
import config
#import phpserialize
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method
from sqlalchemy.ext.associationproxy import association_proxy

# Set default timezone
tzzz = {'EST': gettz('America/New_York')}

Base = declarative_base()

meta_engine = create_engine(config.drupal_connection_string)
session = sessionmaker(meta_engine)()
metadata = MetaData(bind=meta_engine)

#Reflect each database table we need to use, using metadata
class Taxonomy_Term(Base):
    __table__ = Table('taxonomy_term_data', metadata, autoload=True)

class Url_Alias(Base):
    __table__ = Table('url_alias', metadata, autoload=True)


class Node(Base):
    __table__ = Table('node', metadata, autoload=True)

    @hybrid_property
    def tags(self):
        return set([v.term_name for v in self.full_topic + self.topic_wamu + self.topic_wamu_primary if v is not None])

    @hybrid_property
    def drupal_url(self):
        return 'node/' + str(self.nid)

    @hybrid_property
    def body(self):
        try:
            return self._body[0].body_value
        except:
            return ''

    @hybrid_property
    def teaser(self):
        try:
            return self._teaser[0].field_teaser_value
        except:
            return ''

    @hybrid_property
    def date_publish(self):
        try:
            return parse(self._date_publish[0].field_date_publish_value + ' EST', tzinfos=tzzz)
        except:
            return datetime.utcfromtimestamp(self.created)

    @hybrid_property
    def date_publish_gmt(self):
        try:
            return self.date_publish.astimezone(tzutc())
        except:
            return self.date_publish

    @hybrid_property
    def channel(self):
        try:
            return self._channel[0].field_channel_tid
        except:
            return ''

    @hybrid_property
    def wamu_audio(self):
        try:
            return self._wamu_audio[0].field_wamu_audio_value
        except:
            return None

    @hybrid_property
    def url_audio_feed(self):
        try:
            return self._url_audio_feed[0].field_url_audio_feed_value
        except:
            return None

  #   @hybrid_property
  #   def image_feed_url_enlarge(self):
        # try:
        #   if len(self._image_feed_url_enlarge[0].field_image_feed_url_enlarge_value) > 0:
        #       return self._image_feed_url_enlarge[0].field_image_feed_url_enlarge_value
        # except:
        #   return None

    @hybrid_property
    def image_feed_url_wide(self):
        try:
            return self._image_feed_url_wide[0].field_image_feed_url_wide_value if (len(self._image_feed_url_wide[0].field_image_feed_url_wide_value) > 0) else None 
        except:
            return None

    @hybrid_property
    def url_alias(self):
        try:
            return session.query(Url_Alias).filter_by(source='node/' + str(self.nid)).limit(1)[0].alias.split('/')[-1].split('.')[0]
        except:
            return slugify(self.title)

    @hybrid_property
    def image_attach(self):
        try:
            return self._image_attach[0]
        except:
            return None

    @hybrid_property
    def author(self):
        try:
            return [int(self._author[0].nid)]
        except:
            return [None]

    @hybrid_property
    def author_name(self):
        try:
            return [i.field_author_name_value for i in self._author_name]
        except:
            return None

    @hybrid_property
    def link_related_url(self):
        try:
            return [i.field_link_related_url for i in self._link_related]
        except:
            return None

    @hybrid_property
    def link_related_title(self):
        try:
            return [i.field_link_related_title for i in self._link_related]
        except:
            return None


    @hybrid_property
    def program(self):
        try:
            return [int(self._program[0].field_program_nid)]
        except:
            return [None]

    @hybrid_property
    def author(self):
        try:
            return [int(self._author[0].field_author_nid)]
        except:
            return [None]

    @hybrid_property
    def full_topic(self):
        if len(self._full_topic) > 0:
            return self._full_topic
        else:
            return []

    @hybrid_property
    def topic_wamu(self):
        if len(self._topic_wamu) > 0:
            return self._topic_wamu
        else:
            return []
            

    @hybrid_property
    def topic_wamu_primary(self):
        if len(self._topic_wamu_primary) > 0:
            return self._topic_wamu_primary
        else:
            return []

    @hybrid_property
    def subtitle(self):
        try:
            return self._subtitle[0].field_subtitle_value  if (len(self._subtitle[0].field_subtitle_value) > 0) else None 
        except:
            return None
            

    @hybrid_property
    def extra_content(self):
        try:
            return self._extra_content[0].field_extra_content_value if (len(self._extra_content[0].field_extra_content_value) > 0) else None
        except:
            return None

    @hybrid_property
    def image_feed_caption(self):
        try:
            return self._image_feed_caption[0].field_image_feed_caption_value  if (len(self._image_feed_caption[0].field_image_feed_caption_value) > 0) else None 
        except:
            return None

    @hybrid_property
    def image_feed_title(self):
        try:
            return self._image_feed_title[0].field_image_feed_title_value if (len(self._image_feed_title[0].field_image_feed_title_value) > 0) else None 
        except:
            return None

    @hybrid_property
    def image_feed_producer(self):
        try:
            return self._image_feed_producer[0].field_image_feed_producer_value if (len(self._image_feed_producer[0].field_image_feed_producer_value) > 0) else None 
        except:
            return None

    @hybrid_property
    def image_feed_provider(self):
        try:
            return self._image_feed_provider[0].field_image_feed_provider_value if (len(self._image_feed_provider[0].field_image_feed_provider_value) > 0) else None 
        except:
            return None

    @hybrid_property
    def image_feed_copyright(self):
        try:
            return self._image_feed_copyright[0].field_image_feed_copyright_value  if (len(self._image_feed_copyright[0].field_image_feed_copyright_value) > 0) else None 
        except:
            return None

    @hybrid_property
    def meta(self):

        meta_dict = {}

        meta_mapper = {
            'nid' : '_nid',
            'wamu_audio' : 'wamu_audio',
            'url_audio_feed' : 'url_audio_feed',
            'image_feed_url_wide' : 'image_feed_url_wide',
            'image_feed_caption' : 'image_feed_caption',
            'image_feed_title' : 'image_feed_title',
            'image_feed_producer' : 'image_feed_producer',
            'image_feed_provider' : 'image_feed_provider',
            'image_feed_copyright' : 'image_feed_copyright',
            'extra_content' : 'extra_content',
            'subtitle': 'subtitle',
            'author_name': 'author_name',
            'link_related_title': 'link_related_title',
            'link_related_url': 'link_related_url'
        }

        for k, v in meta_mapper.items():
            if self.__getattribute__(k) is not None:
                temp = self.__getattribute__(k)
                if type(temp) is list:
                    for i,j in enumerate(temp):
                        meta_dict[v + '_' + str(i)] = j
                else:
                    meta_dict[v] = temp

        return meta_dict


    @hybrid_property
    def children(self):

        children = []

        # attach images as children
        if self.image_attach is not None:
            children.append({
                'post_title': self.image_attach.title, 
                'post_type': 'attachment', 
                'post_date': self.date_publish, 
                'post_date_gmt': self.date_publish_gmt,
                'post_content': '',
                'post_excerpt': self.image_attach.excerpt,
                'post_modified': self.date_publish, 
                'post_modified_gmt': self.date_publish_gmt,
                'guid': self.image_attach.filename,
                'post_name': self.image_attach.filename,
                'post_mime_type' : self.image_attach.filemime,
                'meta' : self.image_attach.meta
            })

        return children


    _body = relationship("Body",
                    primaryjoin="Node.nid==foreign(Body.entity_id)")
    _wamu_audio = relationship("Wamu_Audio",
                    primaryjoin="Node.nid==foreign(Wamu_Audio.entity_id)")
    _teaser = relationship("Teaser",
                    primaryjoin="Node.nid==foreign(Teaser.entity_id)")
    _url_audio_feed = relationship("Url_Audio_Feed",
                    primaryjoin="Node.nid==foreign(Url_Audio_Feed.entity_id)")
    _channel = relationship("Channel",
                    primaryjoin="Node.nid==foreign(Channel.entity_id)")
    # _image_feed_url_enlarge = relationship("Image_Feed_Url_Enlarge",
    #                 primaryjoin="Node.nid==foreign(Image_Feed_Url_Enlarge.entity_id)")
    _image_attach = relationship("Image_Attach",
                    primaryjoin="Node.nid==foreign(Image_Attach.entity_id)")    
    # does this have to be cast as datetime somewhere????
    _date_publish = relationship("Publish_Date",
                    primaryjoin="Node.nid==foreign(Publish_Date.entity_id)")
    _image_feed_url_wide = relationship("Image_Feed_Url_Wide",
                    primaryjoin="Node.nid==foreign(Image_Feed_Url_Wide.entity_id)")
    _image_feed_producer = relationship("Image_Feed_Producer",
                    primaryjoin="Node.nid==foreign(Image_Feed_Producer.entity_id)")
    _image_feed_provider = relationship("Image_Feed_Provider",
                    primaryjoin="Node.nid==foreign(Image_Feed_Provider.entity_id)")
    _image_feed_copyright = relationship("Image_Feed_Copyright",
                    primaryjoin="Node.nid==foreign(Image_Feed_Copyright.entity_id)")
    _image_feed_caption = relationship("Image_Feed_Caption",
                    primaryjoin="Node.nid==foreign(Image_Feed_Caption.entity_id)")
    _image_feed_title = relationship("Image_Feed_Title",
                    primaryjoin="Node.nid==foreign(Image_Feed_Title.entity_id)")
    _extra_content = relationship("Extra_Content",
                    primaryjoin="Node.nid==foreign(Extra_Content.entity_id)")
    _subtitle = relationship("Subtitle",
                    primaryjoin="Node.nid==foreign(Subtitle.entity_id)")
    _date_modified = relationship("Date_Modified",
                    primaryjoin="Node.nid==foreign(Date_Modified.entity_id)")
    _author_name = relationship("Author_Name",
                    primaryjoin="Node.nid==foreign(Author_Name.entity_id)")
    _author = relationship("Author",
                    primaryjoin="Node.nid==foreign(Author.entity_id)")
    _program = relationship("Program",
                    primaryjoin="Node.nid==foreign(Program.entity_id)")
    _full_topic = relationship("Full_Topic",
                    primaryjoin="Node.nid==foreign(Full_Topic.entity_id)")
    _topic_wamu = relationship("Topic_Wamu",
                    primaryjoin="Node.nid==foreign(Topic_Wamu.entity_id)")
    _topic_wamu_primary = relationship("Topic_Wamu_Primary",
                    primaryjoin="Node.nid==foreign(Topic_Wamu_Primary.entity_id)")
    _link_related = relationship("Link_Related",
                  primaryjoin="Node.nid==foreign(Link_Related.entity_id)")

    # id_origin = relationship("Id_Origin",
    #                 primaryjoin="Node.nid==foreign(Id_Origin.entity_id)")
    # feed_provider_comp = relationship("Feed_Provider_Comp",
    #                 primaryjoin="Node.nid==foreign(Feed_Provider_Comp.entity_id)")
    # url_origin_api = relationship("Url_Origin_Api",
    #                 primaryjoin="Node.nid==foreign(Url_Origin_Api.entity_id)")
    # pullquote = relationship("Pullquote",
    #                 primaryjoin="Node.nid==foreign(Pullquote.entity_id)")
    # image_feed_producer = relationship("Image_Feed_Producer",
    #                 primaryjoin="Node.nid==foreign(Image_Feed_Producer.entity_id)")
    # image_feed_link = relationship("Image_Feed_Link",
    #                 primaryjoin="Node.nid==foreign(Image_Feed_Link.entity_id)")
    # image_feed_url_crop = relationship("Image_Feed_Url_Crop",
    #                 primaryjoin="Node.nid==foreign(Image_Feed_Url_Crop.entity_id)")
    # image_feed_url_wide = relationship("Image_Feed_Url_Wide",
    #                 primaryjoin="Node.nid==foreign(Image_Feed_Url_Wide.entity_id)")
    # image_feed_url_sq = relationship("Image_Feed_Url_Sq",
    #                 primaryjoin="Node.nid==foreign(Image_Feed_Url_Sq.entity_id)")
    # url_origin = relationship("Url_Origin",
    #                 primaryjoin="Node.nid==foreign(Url_Origin.entity_id)")
    # organization = relationship("Organization",
    #                 primaryjoin="Node.nid==foreign(Organization.entity_id)")
    # author = relationship("Author",
    #                 primaryjoin="Node.nid==foreign(Author.entity_id)")
    # teaser_short = relationship("Teaser_Short",
    #                 primaryjoin="Node.nid==foreign(Teaser_Short.entity_id)")
    # title_short = relationship("Title_Short",
    #                 primaryjoin="Node.nid==foreign(Title_Short.entity_id)")
    # image_feed_url = relationship("Image_Feed_Url",
    #                 primaryjoin="Node.nid==foreign(Image_Feed_Url.entity_id)")
    # image_feed_id = relationship("Image_Feed_Id",
    #                 primaryjoin="Node.nid==foreign(Image_Feed_Id.entity_id)")
    # author_name = relationship("Author_Name",
    #                 primaryjoin="Node.nid==foreign(Author_Name.entity_id)")

    # related_article = relationship("Related_Article",
    #                 primaryjoin="Node.nid==foreign(Related_Article.entity_id)")

class Body(Base):
    __table__ = Table('field_data_body', metadata, autoload=True)

class Wamu_Audio(Base):
    __table__ = Table('field_data_field_wamu_audio', metadata, autoload=True)

class Teaser(Base):
    __table__ = Table('field_data_field_teaser', metadata, autoload=True)

class Publish_Date(Base):
    __table__ = Table('field_data_field_date_publish', metadata, autoload=True)

class Image_Feed_Provider(Base):
    __table__ = Table('field_data_field_image_feed_provider', metadata, autoload=True)

class Image_Feed_Copyright(Base):
    __table__ = Table('field_data_field_image_feed_copyright', metadata, autoload=True)

class Extra_Content(Base):
    __table__ = Table('field_data_field_extra_content', metadata, autoload=True)

class Id_Origin(Base):
    __table__ = Table('field_data_field_id_origin', metadata, autoload=True)

class Feed_Provider_Comp(Base):
    __table__ = Table('field_data_field_feed_provider_comp', metadata, autoload=True)

class Url_Origin_Api(Base):
    __table__ = Table('field_data_field_url_origin_api', metadata, autoload=True)

class Pullquote(Base):
    __table__ = Table('field_data_field_pullquote', metadata, autoload=True)

class Topic_Wamu(Base):
    __table__ = Table('field_data_field_topic_wamu', metadata, autoload=True)

    @hybrid_property
    def term_name(self):
        try:
            return self._term[0].name
        except:
            return None


    _term = relationship("Taxonomy_Term", primaryjoin="Topic_Wamu.field_topic_wamu_tid==foreign(Taxonomy_Term.tid)")

class Full_Topic(Base):
    __table__ = Table('field_data_field_full_topic', metadata, autoload=True)

    @hybrid_property
    def term_name(self):
        try:
            return self._term[0].name
        except:
            return None


    _term = relationship("Taxonomy_Term", primaryjoin="Full_Topic.field_full_topic_tid==foreign(Taxonomy_Term.tid)")

class Topic_Wamu_Primary(Base):
    __table__ = Table('field_data_field_topic_wamu_primary', metadata, autoload=True)

    @hybrid_property
    def term_name(self):
        try:
            return self._term[0].name
        except:
            return None

    _term = relationship("Taxonomy_Term", primaryjoin="Topic_Wamu_Primary.field_topic_wamu_primary_tid==foreign(Taxonomy_Term.tid)")

class File_Managed(Base):
    __table__ = Table('file_managed', metadata, autoload=True)

class Image_Attach_Caption(Base):
    __table__ = Table('field_data_field_image_attach_caption', metadata, autoload=True)

# class Image_Attach_Producer(Base):
#   __table__ = Table('field_data_field_image_attach_producer', metadata, autoload=True)

# class Image_Attach_Copyright(Base):
#   __table__ = Table('field_data_field_image_attach_copyright', metadata, autoload=True)

# class Image_Attach_Provider(Base):
#   __table__ = Table('field_data_field_image_attach_provider', metadata, autoload=True)

class Image_Attach_Credit(Base):
    __table__ = Table('field_data_field_image_attach_credit', metadata, autoload=True)

class Image_Attach(Base):
    __table__ = Table('field_data_field_image_attach', metadata, autoload=True)

    @hybrid_property
    def filename(self):
        try:
            return self._file_managed[0].filename
        except:
            return ''

    @hybrid_property
    def filemime(self):
        try:
            return self._file_managed[0].filemime
        except:
            return ''

    @hybrid_property
    def file_managed(self):
        try:
            return self._file_managed[0]
        except:
            return None

    @hybrid_property
    def credit(self):
        try:
            return self._credit[0].field_image_attach_credit_value
        except:
            return None

    @hybrid_property
    def excerpt(self):
        try:
            return self._caption[0].field_image_attach_caption_value
        except:
            return ''

    @hybrid_property
    def title(self):
        try:
            return self._title[0].field_image_attach_title_value
        except:
            return self.filename

    @hybrid_property
    def meta(self):

        meta_dict = {}

        if self.file_managed is not None:
            meta_dict['_wp_attached_file'] = self.file_managed.uri.replace('public://','')
        if self.credit is not None:
            meta_dict['credit'] = self.credit
    
        return meta_dict

    _file_managed = relationship("File_Managed", primaryjoin="Image_Attach.field_image_attach_fid==foreign(File_Managed.fid)")
    _caption = relationship("Image_Attach_Caption", primaryjoin="Image_Attach.entity_id==foreign(Image_Attach_Caption.entity_id)")
    _credit = relationship("Image_Attach_Credit", primaryjoin="Image_Attach.entity_id==foreign(Image_Attach_Credit.entity_id)")
    _title = relationship("Image_Attach_Title", primaryjoin="Image_Attach.entity_id==foreign(Image_Attach_Title.entity_id)")


class Image_Feed_Producer(Base):
    __table__ = Table('field_data_field_image_feed_producer', metadata, autoload=True)

class Image_Feed_Link(Base):
    __table__ = Table('field_data_field_image_feed_link', metadata, autoload=True)

# class Image_Feed_Url_Crop(Base):
#   __table__ = Table('field_data_field_image_feed_url_crop', metadata, autoload=True)

# class Image_Feed_Url_Enlarge(Base):
#   __table__ = Table('field_data_field_image_feed_url_enlarge', metadata, autoload=True)

class Image_Feed_Url_Wide(Base):
    __table__ = Table('field_data_field_image_feed_url_wide', metadata, autoload=True)

# class Image_Feed_Url_Sq(Base):
#   __table__ = Table('field_data_field_image_feed_url_sq', metadata, autoload=True)

class Url_Origin(Base):
    __table__ = Table('field_data_field_url_origin', metadata, autoload=True)

class Organization(Base):
    __table__ = Table('field_data_field_organization', metadata, autoload=True)

class Author(Base):
    __table__ = Table('field_data_field_author', metadata, autoload=True)

# class Computed_Filename(Base):
#   __table__ = Table('field_data_field_computed_filename', metadata, autoload=True)

class Image_Attach_Title(Base):
    __table__ = Table('field_data_field_image_attach_title', metadata, autoload=True)

# class Date_Publish_Comp(Base):
#   __table__ = Table('field_data_field_date_publish_comp', metadata, autoload=True)

class Channel(Base):
    __table__ = Table('field_data_field_channel', metadata, autoload=True)

class Teaser_Short(Base):
    __table__ = Table('field_data_field_teaser_short', metadata, autoload=True)

class Title_Short(Base):
    __table__ = Table('field_data_field_title_short', metadata, autoload=True)

class Subtitle(Base):
    __table__ = Table('field_data_field_subtitle', metadata, autoload=True)

class Date_Modified(Base):
    __table__ = Table('field_data_field_date_modified', metadata, autoload=True)

class Image_Feed_Url(Base):
    __table__ = Table('field_data_field_image_feed_url', metadata, autoload=True)

class Image_Feed_Id(Base):
    __table__ = Table('field_data_field_image_feed_id', metadata, autoload=True)

class Image_Feed_Title(Base):
    __table__ = Table('field_data_field_image_feed_title', metadata, autoload=True)

class Image_Feed_Caption(Base):
    __table__ = Table('field_data_field_image_feed_caption', metadata, autoload=True)

class Url_Audio_Feed(Base):
    __table__ = Table('field_data_field_url_audio_feed', metadata, autoload=True)

class Author_Name(Base):
    __table__ = Table('field_data_field_author_name', metadata, autoload=True)

class Link_Related(Base):
    __table__ = Table('field_data_field_link_related', metadata, autoload=True)

class Related_Article(Base):
    __table__ = Table('field_data_field_related_article', metadata, autoload=True)

class Program(Base):
    __table__ = Table('field_data_field_program', metadata, autoload=True)

class Twitter_Screen_Name(Base):
    __table__ = Table('field_data_field_twitter_screen_name', metadata, autoload=True)

class Author_Jobtitle(Base):
    __table__ = Table('field_data_field_author_jobtitle', metadata, autoload=True)

class Node_Program(Base):
    __table__ = Table('node', metadata, autoload=True)

    # URL ALIAS for slug

    @hybrid_property
    def body(self):
        try:
            return self._body[0].body_value
        except:
            return ''

    _body = relationship("Body", primaryjoin="Node_Program.nid==foreign(Body.entity_id)")

class Node_Author(Base):
    __table__ = Table('node', metadata, autoload=True)

    
    @hybrid_property
    def name(self):
        try:
            return self.title
        except:
            return None

    @hybrid_property
    def body(self):
        try:
            return self._body[0].body_value
        except:
            return None

    @hybrid_property
    def twitter_screen_name(self):
        try:
            return self._twitter_screen_name[0].field_twitter_screen_name_value
        except:
            return None

    @hybrid_property
    def jobtitle(self):
        try:
            return self._jobtitle[0].field_author_jobtitle_value
        except:
            return None

    _twitter_screen_name = relationship("Twitter_Screen_Name", primaryjoin="Node_Author.nid==foreign(Twitter_Screen_Name.entity_id)")
    _body = relationship("Body", primaryjoin="Node_Author.nid==foreign(Body.entity_id)")
    _jobtitle = relationship("Author_Jobtitle", primaryjoin="Node_Author.nid==foreign(Author_Jobtitle.entity_id)")


